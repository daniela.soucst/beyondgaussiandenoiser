# -*- coding: utf-8 -*-
from __future__ import print_function, division
import os
import torch
from skimage import data, transform
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import PIL
from skimage.util import noise
import numpy as np
import cv2


class SpeckleDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list, transform):
        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
            real_illu (numpy array)
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        # self.kgroup_list = kgroup_list
        self.transform = transform
        # self.real_rgb_list = real_illu

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.img_names_list[idx])
#         print(img_name)
        clean_image = np.asarray(cv2.imread(img_name, 0))
#         clean_image = data.imread(img_name,as_gray=True)
 

        speckle_image = noise.random_noise(clean_image, mode='speckle')
    
        input_img = PIL.Image.fromarray(speckle_image)
        output_img = PIL.Image.fromarray(clean_image)

        sample = {'speckle_image': input_img, 'clean_image': output_img}

        if self.transform:
#             sample = self.transform(sample)
              sample['speckle_image'] = self.transform(sample['speckle_image'])
              sample['clean_image'] = self.transform(sample['clean_image'])

        return sample
