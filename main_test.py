%%writefile beyondgaussiandenoiser/main_test.py
# -*- coding: utf-8 -*-
import argparse
import os, time, datetime
# import PIL.Image as Image
import numpy as np
import torch.nn as nn
import torch.nn.init as init
import torch
from skimage.measure import compare_psnr, compare_ssim
from skimage.io import imread, imsave
from model import ID_ResNet #Modelo ID_ResNet
from dataset import SpeckleDataset
import torch.backends.cudnn as cudnn
from skimage.util import noise
import cv2

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--set_dir', default='/content/gdrive/My Drive/Deep_Learning/Dataset', type=str, help='directory of test dataset')
    parser.add_argument('--set_names', default=['Test3'], help='directory of test dataset')
    parser.add_argument('--sigma', default=25, type=int, help='noise level')
    parser.add_argument('--model_dir', default='/content/gdrive/My Drive/Deep_Learning/checkpoint', help='directory of the model')
    parser.add_argument('--model_name', default='checkpoint_500.pth', type=str, help='the model name')
    parser.add_argument('--result_dir', default='/content/gdrive/My Drive/Deep_Learning/results/speackle', type=str, help='directory of test dataset')
    parser.add_argument('--save_result', default=1, type=int, help='save the denoised image, 1 or 0')
    return parser.parse_args()


# def log(*args, **kwargs):
#      print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S:"), *args, **kwargs)


def save_result(result, path):
    path = path if path.find('.') != -1 else path+'.png'
    ext = os.path.splitext(path)[-1]
    if ext in ('.txt', '.dlm'):
        np.savetxt(path, result, fmt='%2.4f')
    else:
        imsave(path, np.clip(result, 0, 1))


def show(x, title=None, cbar=False, figsize=None):
    import matplotlib.pyplot as plt
    plt.figure(figsize=figsize)
    plt.imshow(x, interpolation='nearest', cmap='gray')
    if title:
        plt.title(title)
    if cbar:
        plt.colorbar()
    plt.show()



args = parse_args()
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>LOADING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

model = ID_ResNet()

device = 'cuda' if torch.cuda.is_available() else 'cpu'

model = model.to(device)
if device == 'cuda':
    model = torch.nn.DataParallel(model)
    cudnn.benchmark = True

if not os.path.exists(os.path.join(args.model_dir, args.model_name)):  
  print('Model not found!')
  

        
else:  
  checkpoint = torch.load('/content/gdrive/My Drive/Deep_Learning/checkpoint/checkpoint_500.pth')
  model.load_state_dict(checkpoint['net'])
  print('load trained model',os.path.join(args.model_dir, args.model_name))


# print(model)
model.eval()  # evaluation mode


if not os.path.exists(args.result_dir):
  os.mkdir(args.result_dir)

for set_cur in args.set_names:

        if not os.path.exists(os.path.join(args.result_dir, set_cur)):
            os.mkdir(os.path.join(args.result_dir, set_cur))
        psnrs = []
        ssims = []

        for im in os.listdir(os.path.join(args.set_dir, set_cur)):
            if im.endswith(".jpg") or im.endswith(".bmp") or im.endswith(".png"):

#                 x = np.array(imread(os.path.join(args.set_dir, set_cur, im), as_gray=False), dtype=np.float32)/255.0
                x = np.array(cv2.imread(os.path.join(args.set_dir, set_cur, im), 0), dtype=np.float32)/255.0
#                 print ('img: ',os.path.join(args.set_dir, set_cur, im),x.shape)
                np.random.seed(seed=0)  # for reproducibility
#                 y = x + np.random.normal(0, args.sigma/255.0, x.shape)  # Add Gaussian noise without clipping
                y = noise.random_noise(x, mode='speckle')
                y = y.astype(np.float32)
                y_ = torch.from_numpy(y).view(1, -1, y.shape[0], y.shape[1])

                torch.cuda.synchronize()
                start_time = time.time()
                y_ = y_.cuda()
                x_ = model(y_)  # inference
                x_ = x_.view(y.shape[0], y.shape[1])
                x_ = x_.cpu()
                x_ = x_.detach().numpy().astype(np.float32)
                torch.cuda.synchronize()
                elapsed_time = time.time() - start_time
                print('%10s : %10s : %2.4f second' % (set_cur, im, elapsed_time))

                psnr_x_ = compare_psnr(x, x_)
                ssim_x_ = compare_ssim(x, x_)
                if args.save_result:
                    name, ext = os.path.splitext(im)
#                     show(np.hstack((y, x_)))  # show the image
                    save_result(x_, path=os.path.join(args.result_dir, set_cur, name+'_dncnn'+ext))  # save the denoised image
                    save_result(y, path=os.path.join(args.result_dir, 'noise_images', name+'_noise'+ext))
                psnrs.append(psnr_x_)
                ssims.append(ssim_x_)
                
psnr_avg = np.mean(psnrs)
ssim_avg = np.mean(ssims)
psnrs.append(psnr_avg)
ssims.append(ssim_avg)
if args.save_result:
  save_result(np.hstack((psnrs, ssims)), path=os.path.join(args.result_dir, set_cur, 'results.txt'))
print('Datset: {0:10s} \n  PSNR = {1:2.2f}dB, SSIM = {2:1.4f}'.format(set_cur, psnr_avg, ssim_avg))        
    
