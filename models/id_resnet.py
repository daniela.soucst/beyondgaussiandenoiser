import torch
import torch.nn as nn
import torch.nn.functional as F

class ID_ResNet(nn.Module):
    expansion = 1

    def __init__(self, kernel_size=3, stride=1, padding=0):
        super(ID_ResNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 64, kernel_size=kernel_size, stride=stride, padding=padding)

        self.conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=kernel_size, padding=padding)
        self.conv2_bn = nn.BatchNorm2d(64)

        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=kernel_size, padding=padding)
        self.conv3_bn = nn.BatchNorm2d(64)

        self.conv4 = nn.Conv2d(64, 64, kernel_size=3, stride=kernel_size, padding=padding)
        self.conv4_bn = nn.BatchNorm2d(64)

        self.conv5 = nn.Conv2d(64, 64, kernel_size=3, stride=kernel_size, padding=padding)
        self.conv5_bn = nn.BatchNorm2d(64)

        self.conv6 = nn.Conv2d(64, 64, kernel_size=3, stride=kernel_size, padding=padding)
        self.conv6_bn = nn.BatchNorm2d(64)

        self.conv7 = nn.Conv2d(64, 64, kernel_size=3, stride=kernel_size, padding=padding)
        self.conv7_bn = nn.BatchNorm2d(64)

        self.conv8 = nn.Conv2d(64, 1, kernel_size=3, stride=kernel_size, padding=padding)

        self.tanh = nn.Tanh()

        # self.shortcut = nn.Sequential()
        #
        # if stride != 1 or in_planes != self.expansion * planes:
        #     self.shortcut = nn.Sequential(
        #         nn.Conv2d(in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
        #         nn.BatchNorm2d(self.expansion * planes)
        #     )

    def forward(self, x):
        x = x.float()
        print(x.size())
        out = F.relu(self.conv1(x))  # L1
        print(out.size())
        out = F.relu(self.conv2_bn(self.conv2(out)))
        print(out.size())
        out = F.relu(self.conv3_bn(self.conv3(out)))
        print(out.size())
        out = F.relu(self.conv4_bn(self.conv4(out)))
        print(out.size())
        out = F.relu(self.conv5_bn(self.conv5(out)))
        print(out.size())
        out = F.relu(self.conv6_bn(self.conv6(out)))
        print(out.size())
        out = F.relu(self.conv7_bn(self.conv7(out)))
        print(out.size())
        out = F.relu(self.conv8(out))
        print(out.size())
        # torch.addcdiv(0, 1, t1, t2)
        out = x / out
        out = self.tanh(out)

        return out