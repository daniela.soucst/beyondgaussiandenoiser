%%writefile beyondgaussiandenoiser/main_vitoria.py
# -*- coding: utf-8 -*-
from __future__ import print_function, division
import os
import torch
from skimage import data, transform
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torch.nn as nn
# from model import SDI_Net #Modelo SDI_Net
from sdi_net import SDI_Net
from dataset import SpeckleDataset
import PIL
from skimage.util import noise
import argparse
import glob
import torch.backends.cudnn as cudnn
import torch.optim as optim
from utils import progress_bar
import cv2
import numpy as np
from google.colab import drive
import torchvision.models as models
from collections import OrderedDict
import torch.utils.model_zoo as model_zoo
import utils


################################

plt.ion()  # interactive mode
perceptive_mode = False

parser = argparse.ArgumentParser(description='PyTorch CIFAR10 Training')
parser.add_argument('--lr', default=0.0002, type=float, help='learning rate')
parser.add_argument('--dirTrain', default='/content/gdrive/My Drive/Deep_Learning/Dataset/Train97', type=str, help='diretorio das imagens treinamento')
parser.add_argument('--dirTest', default='/content/gdrive/My Drive/Deep_Learning/Dataset/Test3', type=str, help='diretorio das imagens teste')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--seed', type=int, default=1, metavar='S', help='random seed (default: 1)')
args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
start_epoch = 0  # start from epoch 0 or last checkpoint epoch
torch.manual_seed(args.seed)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>UTILITÁRIOS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

def getPerceptiveLoss(features_vgg1, features_vgg2):  

    d = features_vgg1.shape[1]; h = features_vgg1.shape[2]; w = features_vgg1.shape[3]
    dist = torch.dist(features_vgg1, features_vgg2)
    loss = (dist**2)/(h*w*d)    
    return loss
  
# def tvloss(y):
  
#   bsize, chan, height, width = y.size()
#   errors = []
#   for w in range(width-1):
#     for h in range(height-1):
#       dy = torch.abs(y[:,:,h,w+1] - y[:,:,h,w])
#       dx = torch.abs(y[:,:,h+1,w] - y[:,:,h,w])
#       error = torch.norm(dy - dx, 1)
#       errors.append(error)

#   return sum(errors) / (height * width)

class TVLoss(nn.Module):
  
    def __init__(self,TVLoss_weight=1):
        super(TVLoss,self).__init__()
        self.TVLoss_weight = TVLoss_weight

    def forward(self,x):
        batch_size = x.size()[0]
        h_x = x.size()[2]
        w_x = x.size()[3]
        count_h = self._tensor_size(x[:,:,1:,:])
        count_w = self._tensor_size(x[:,:,:,1:])
        h_tv = torch.pow((x[:,:,1:,:]-x[:,:,:h_x-1,:]),2).sum()
        w_tv = torch.pow((x[:,:,:,1:]-x[:,:,:,:w_x-1]),2).sum()
        return self.TVLoss_weight*2*(h_tv/count_h+w_tv/count_w)/batch_size

    def _tensor_size(self,t):
        return t.size()[1]*t.size()[2]*t.size()[3]

def findLastCheckpoint(save_dir):
  
    dir_len = len(save_dir) + len("checkpoint_")
  
    file_number_list = []
    
    files = glob.glob(os.path.join(save_dir, 'checkpoint_*.pth'))
    
    if len(files) > 0:
    
      for file_number in files:
        file_number_list.append(int(file_number[dir_len:-4]))

      last_epoch = max(file_number_list)

      last_checkpoint_filename = "checkpoint_" + str(last_epoch) + ".pth"

      return last_epoch, last_checkpoint_filename
    
    else:
      
      return 0, ""
    

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PERCEPTUAL FEATURES<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

vgg19 = models.vgg19()
vgg19.load_state_dict(model_zoo.load_url('https://download.pytorch.org/models/vgg19-dcbb9e9d.pth'))



print ("Carregou pesos...")
class VGG1_1(nn.Module):
            def __init__(self):
                super(VGG1_1, self).__init__()
                self.features = nn.Sequential(
                    # stop at second RELU before the first pooling layer
                    *list(vgg19.features.children())[:-33]
                )
            def forward(self, x):
                x = x.float()
                x = self.features(x)
                return x

vgg19_2_out = VGG1_1()
vgg19_2_label = VGG1_1()


# #na vgg carregada, a entrada é para imagens com 3 canais. Por isso, adaptamos para 1
vgg19_2_out.features[0] = nn.Conv2d(1, 64, kernel_size=(3,3), stride=(1,1), padding=(1,1)) 
vgg19_2_label.features[0]= nn.Conv2d(1, 64, kernel_size=(3,3), stride=(1,1), padding=(1,1))


# print (vgg19_2_out)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

vgg19_2_out = vgg19_2_out.to(device)
vgg19_2_label = vgg19_2_label.to(device)
if device == 'cuda':
    vgg19_2_out = torch.nn.DataParallel(vgg19_2_out)
    vgg19_2_label = torch.nn.DataParallel(vgg19_2_label)
    cudnn.benchmark = True   
    
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PREPARING DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# average_color,std_color = utils.get_mean_stDeviation(args.dirTrain) #[0.48369651 0.48964715 0.44966717] [0.17346853 0.16349056 0.15546544]
 
print('==> Preparing data..')
transform_train = transforms.Compose([
    transforms.RandomCrop(256, padding=1),
#     transforms.RandomCrop(128, padding=1),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.48369651, 0.48964715, 0.44966717), (0.17346853, 0.16349056, 0.15546544)),
])

transform_test = transforms.Compose([      
    transforms.ToTensor(),
    transforms.Normalize((0.48369651, 0.48964715, 0.44966717), (0.17346853, 0.16349056, 0.15546544)),
])


train_dataset = SpeckleDataset(root_dir=args.dirTrain, img_names_list=glob.glob(args.dirTrain + '/*.bmp'),
                               transform=transform_train)

trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True, num_workers=2)

test_dataset = SpeckleDataset(root_dir=args.dirTest, img_names_list=glob.glob(args.dirTest + '/*.bmp'),
                              transform=transform_test)

testloader = DataLoader(test_dataset, batch_size=16, shuffle=True, num_workers=1)



#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Building model..')

net = SDI_Net()
net = net.to(device)
if device == 'cuda':
    net = torch.nn.DataParallel(net)
    cudnn.benchmark = True

# last_epoch, last_checkpoint_filename = findLastCheckpoint('/content/gdrive/My Drive/Colab Notebooks/Deep_Learning/checkpoint/')
checkpoint = torch.load('/content/gdrive/My Drive/Deep_Learning/checkpoint/checkpoint_500.pth')

if checkpoint != None:  
#     last_epoch = last_epoch + 1
    last_epoch = checkpoint['epoch']
    print('resuming by loading' + str(last_epoch))
#     checkpoint = torch.load('/content/gdrive/My Drive/Deep_Learning/checkpoint/' + last_checkpoint_filename)
    
    net.load_state_dict(checkpoint['net'])
    best_acc = checkpoint['acc']


  

criterion = nn.MSELoss()
tvloss = TVLoss()

# optimizer = optim.SGD(net.parameters(), lr=0.1, momentum=0.9, weight_decay=5e-4)
optimizer = optim.Adam(net.parameters(), lr=args.lr) #WANG (divisao) lr =  0.0002



#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TRAINING<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

def train(epoch):
    print('\nEpoch: %s' % str(epoch))
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    for batch_idx, sample_batched in enumerate(trainloader):
        
        inputs, targets = sample_batched['speckle_image'].to(device), sample_batched['clean_image'].to(device)       
   
        optimizer.zero_grad()
        outputs = net(inputs)
       

        """-------------------PERCEPTIVE FEATURES-------------------"""
        if perceptive_mode:
          out = vgg19_2_out(outputs)
          targ = vgg19_2_label(targets)      
          loss = getPerceptiveLoss(out,targ)
#           print ('loss perceptiva',loss)
        else:
#           loss = criterion(outputs, targets)  #MSELoss   
            loss = criterion(outputs, targets) + (0.002)*tvloss(outputs)  #MSE LOSS + TV LOSS 
#           print ('loss NORMAL',loss,type(loss))
               
      
      
        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
       
        total += targets.size(0)
        correct += predicted.eq(targets.long()).sum().item()

        progress_bar(batch_idx, len(trainloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
                     % (train_loss / (batch_idx + 1), 100. * correct / total, correct, total))
        return correct
      
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TEST<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

def test(epoch,total_epoch):
    global best_acc
    net.eval()
    test_loss = 0
    correct = 0
    total = 0
    mse_loss = 0
    
    with torch.no_grad():
        for batch_idx, sample_batched in enumerate(testloader):            
            inputs, targets = sample_batched['speckle_image'].to(device), sample_batched['clean_image'].to(device)
#             inputs, targets = sample_batched['speckle_image'], sample_batched['clean_image']

            outputs = net(inputs)
    
    
            """-------------------PERCEPTIVE FEATURES-------------------"""
            if perceptive_mode:
              outputs = vgg19_2_out(outputs)
              targ = vgg19_2_label(targets)      
#               loss = getPerceptiveLoss(outputs,targ)
#               print ('loss perceptiva teste',loss.item())
            else:
#               loss = criterion(outputs, targets)   
              loss = criterion(outputs, targets) + (0.002)*tvloss(outputs)  #MSE LOSS + TV LOSS
#               print ('loss NORMAL teste',loss)

           
          
            test_loss += loss.item()
            _, predicted = outputs.max(1)
            
            
            total += targets.size(0)
            correct += predicted.eq(targets.long()).sum().item()
#             print (total,correct)
            
#             progress_bar(batch_idx, len(testloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
#                          % (test_loss / (batch_idx + 1), 100. * correct / total, correct, total))
            mse_loss = (test_loss / (batch_idx + 1))
            progress_bar(batch_idx, len(testloader), 'MSELoss: %.3f '% mse_loss)
            
#     Save checkpoint.
    acc = 100.*(correct / total)
    print('\nEpoch: '+str(epoch)+'| mse_loss:'+str(mse_loss))
#     print("ACC: ", acc)
#     print("BEST_ACC: ", best_acc)
    if (epoch%3==0):
        print('Saving..')
        state = {
            'net': net.state_dict(),
            'mse_loss': mse_loss,
            'epoch': epoch,
            'acc': acc
        }
        if not os.path.isdir('checkpoint'):
            os.mkdir('checkpoint')
        print("CHECKPOINT - EPOCH: ", epoch)
        #torch.save(state, '/content/gdrive/My Drive/Colab Notebooks/Deep_Learning/checkpoint/ckpt.t7')
        best_acc = acc
        torch.save(state, '/content/gdrive/My Drive/Deep_Learning/checkpoint/' + 'checkpoint_' + str(total_epoch) + '.pth')
#     else:
#         state = {
#             'net': net.state_dict(),
#             'mse_loss': mse_loss,
#             'epoch': epoch,
#             'acc': best_acc
#         }
#         torch.save(state, '/content/gdrive/My Drive/Deep_Learning/checkpoint/' + 'checkpoint_' + str(total_epoch) + '.pth')


for epoch in range(start_epoch + last_epoch , 500):
    total_epoch = max(start_epoch + last_epoch , 500)
    train(epoch)
    #print('\targets size:',correct)
    test(epoch,total_epoch)

# for epoch in range(start_epoch, start_epoch + 100):    
#     train(epoch)
#     test(epoch)