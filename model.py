%%writefile beyondgaussiandenoiser/model.py
# -*- coding: utf-8 -*-
from torchvision import transforms, utils
import torch.nn as nn
import torch.nn.functional as F


"""MODEL"""

class SDI_Net(nn.Module):
    

    def __init__(self, kernel_size=3, stride=1, padding=1,depth=8):
        super(SDI_Net, self).__init__()        
        
        layers = []        

        layers.append(nn.Conv2d(1, 64, kernel_size=kernel_size, stride=stride, padding=padding))
        layers.append(nn.ReLU(inplace=True))
        
        for _ in range(depth-2):
            layers.append(nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding))
            layers.append(nn.BatchNorm2d(64))
            layers.append(nn.ReLU(inplace=True))
            
        layers.append(nn.Conv2d(64, 1, kernel_size=kernel_size, stride=stride, padding=padding))
        layers.append(nn.ReLU(inplace=True))
        layers.append(nn.Tanh())
        self.module_list = nn.ModuleList(layers)
#         self.dncnn = nn.Sequential(*layers)
#         if isinstance(self.module_list[0], nn.Conv2d):
#                print('CONv achada!')
       
        
    def forward(self, x):        
        residual = x
        
        out = self.module_list[0](x) #camada 0
        out = res = self.module_list[1](out)#camada 1
        
        next_indice = 5 
        for i in range(2,len(self.module_list)-1):
          if(i==next_indice and isinstance(self.module_list[i], nn.Conv2d)):             
            out_res = out + res   # igual a out += res          
            out = self.module_list[i](out_res)
            res = out_res 
            next_indice += 3
           
          else:
            out = self.module_list[i](out)  

        out = residual / out
        out =  self.module_list[22](out)  #self.tanh(out)
        return out
  
   