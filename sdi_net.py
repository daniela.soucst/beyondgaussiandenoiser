%%writefile beyondgaussiandenoiser/sdi_net.py
# -*- coding: utf-8 -*-
from torchvision import transforms, utils
import torch.nn as nn
import torch.nn.functional as F

class SDI_Net(nn.Module):    

    def __init__(self, kernel_size=3, stride=1, padding=1):
        super(SDI_Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 64, kernel_size=kernel_size, stride=stride, padding=padding)

        self.conv2 = nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv2_bn = nn.BatchNorm2d(64)

        self.conv3 = nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv3_bn = nn.BatchNorm2d(64)

        self.conv4 = nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv4_bn = nn.BatchNorm2d(64)

        self.conv5 = nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv5_bn = nn.BatchNorm2d(64)

        self.conv6 = nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv6_bn = nn.BatchNorm2d(64)

        self.conv7 = nn.Conv2d(64, 64, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv7_bn = nn.BatchNorm2d(64)

        self.conv8 = nn.Conv2d(64, 1, kernel_size=kernel_size, stride=stride, padding=padding)

        self.tanh = nn.Tanh()
        

    def forward(self, x): #faz as camadas residuais de 2 em dois
        x = x.float()
        res = x
      
        out = F.relu(self.conv1(x))  # L1        
        r1 = out

        out = F.relu(self.conv2_bn(self.conv2(out))) #L2
        
        
        out = F.relu(self.conv3_bn(self.conv3(out))) #L3
        out += r1
        out = r2 = F.relu(out)    

        
        out = F.relu(self.conv4_bn(self.conv4(out)))  

        
        out = F.relu(self.conv5_bn(self.conv5(out)))
        out += r2
        out = r3 = F.relu(out)
        

        out = F.relu(self.conv6_bn(self.conv6(out)))

        
        out = F.relu(self.conv7_bn(self.conv7(out)))
        out += r3
        out = F.relu(out)

        out = F.relu(self.conv8(out))
        
#         #Garantir casos de divisão por zero
        out.add_(1e-7)
        
        # torch.addcdiv(0, 1, t1, t2)
        out = res / out
        out = self.tanh(out)

        return out